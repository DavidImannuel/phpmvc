<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?=asset('css/bootstrap.css');?>">
  <script src="<?=asset('js/jquery.js');?>"></script>
	<script src="<?=asset('js/bootstrap.js');?>"></script>
	<script src="<?=asset('js/sweetalert.js');?>"></script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="<?=baseurl();?>">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav">
        <a class="nav-item nav-link active" href="<?=baseurl('example');?>">Example<span class="sr-only"></a>
      </div>
    </div>
  </div>
</nav>