<div class="container">

    <h1><?= $data['title'] ;?></h1>

    <?php Session::flash(); ?>

	<a href="<?=BASEURL;?>example/create" class="btn btn-success">Add Data</a>
    
    <table class="table mt-4" id="table">
	  <thead>
	    <tr>
	      <th>Id</th>
	      <th>Name</th>
	      <th>Email</th>
	      <th>City</th>
	      <th>Phone</th>
	      <th>Option</th>
	    </tr>
	  </thead>
	  <tbody>
        <?php foreach($data['rows'] as $row): ?>
        <tr>
            <td><?= $row['id'] ;?></td>
            <td><?= $row['name'] ;?></td>
            <td><?= $row['email'] ;?></td>
            <td><?= $row['city'] ;?></td>
            <td><?= $row['phone'] ;?></td>
            <td>
				<a href="<?=BASEURL;?>example/edit/<?=$row['id'];?>" class="btn btn-primary">
					Edit
				</a>
				<a href="<?=BASEURL;?>example/delete/<?=$row['id'];?>" class="btn btn-danger" onclick="return confirm('Are you sure ?')">
					Delete
				</a>
			</td>
        </tr>
        <?php endforeach; ?>
	  </tbody>
	</table>

</div>