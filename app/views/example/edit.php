<div class="container">
  <div class="row">
    <div class="col-md-6">
    <form action="<?=baseurl('example/update/'.$data['id']);?>" method="post">
      <div class="form-group">
        <label for="name">Name</label>
        <?php Session::errors('name') ;?>
        <input type="text" class="form-control" id="name" name="name" value="<?=$data['name']?>">
        <label for="email">Email</label>
        <?php Session::errors('email') ;?>
        <input type="text" class="form-control" id="email" name="email" value="<?=$data['email']?>">
        <label for="city">City</label>
        <?php Session::errors('city') ;?>
        <input type="text" class="form-control" id="city" name="city" value="<?=$data['city']?>">
        <label for="phone">Phone</label>
        <?php Session::errors('phone') ;?>
        <input type="text" class="form-control" id="phone" name="phone" value="<?=$data['phone']?>">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </div>
  </div>
</div>