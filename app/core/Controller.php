<?php 

class Controller {

    public function view($view,$data = [])
	{
		require_once ROOT.'/app/views/'.$view.'.php';
	}

	public function model($model)
	{
		require_once ROOT.'/app/models/'.$model.'.php';
		// $obj = 'App\Models\\'.$model;
		return new $model;
	}

	public function helper($helper)
	{
		require_once ROOT.'/app/helpers/'.$helper.'.php';
		return new $helper;
	}
}