<?php 

class DB {

    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $db_name = DB_NAME;

	private $stmt;
	private $pdo;

    public function __construct()
    {
        //data source name
        $dsn = 'mysql:host='.$this->host.';dbname='.$this->db_name;
        try {
            $this->pdo = new PDO($dsn,$this->user,$this->pass);
        } catch(PDOException $e) {
            die($e->getMessage());
        }
    }

	//for SQL query
    public function query($query)
	{
		$this->stmt = $this->pdo->prepare($query);
	}

	public function bind($param,$value,$type = null)
	{
		if (is_null($type)) {
			switch (true) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
					break;
			}
		}

		$this->stmt->bindValue($param,$value,$type);

	}

	public function execute()
	{
		return $this->stmt->execute();
	}

	//for query all rows
	public function resultAll($table)
	{
		$this->stmt = $this->pdo->prepare("SELECT * FROM {$table} ORDER BY id DESC");
		$this->execute();
		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	// for query singgle rows
	public function single($table,$id)
	{
		$this->stmt = $this->pdo->prepare("SELECT * FROM {$table} where id = :id");
		$this->bind('id',$id);
		$this->execute();
		return $this->stmt->fetch(PDO::FETCH_ASSOC);
	}

	//for insert data
	public function insert($table,$fields = [])
	{
		$fieldString = '';
		$valueString = '';

		foreach($fields as $field => $value){
			$fieldString.= "$field,";
			$valueString.= ":$field,";
		}

		$fieldString = rtrim($fieldString,',');
		$valueString = rtrim($valueString,',');
		$sql = "INSERT INTO {$table} ($fieldString) VALUES ({$valueString})";
		
		// var_dump($sql,$values);die;
		$this->query($sql);
		foreach($fields as $field => $value){
			$this->bind($field,$value);
			// echo "$field = $value";
		}
		$this->execute();
	}

	//for update data
	public function update($table,$fields = [],$id)
	{
		$queryString = '';

		foreach($fields as $field => $value){
			$queryString.= "$field = :$field,";
		}

		$queryString = rtrim($queryString,',');
		$sql = "UPDATE {$table} SET {$queryString} WHERE id = :id";
		
		// var_dump($sql);die;
		$this->query($sql);
		foreach($fields as $field => $value){
			$this->bind($field,$value);
			// echo "$field = $value";
		}
		$this->bind('id',$id);
		$this->execute();
	}

	//for delete
	public function delete($table,$id)
	{
			$this->query("DELETE FROM $table WHERE id = :id");
			$this->bind('id',$id,PDO::PARAM_INT);
			$this->execute();

	}
}