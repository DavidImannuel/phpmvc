<?php 
//autoload
spl_autoload_register(function($class){
    $class = explode('\\',$class);
    $class = end($class);
    // var_dump($class);
    if( file_exists(ROOT."/app/core/{$class}.php") ){
        require_once ROOT."/app/core/{$class}.php";
    } else if( file_exists(ROOT."/app/models/{$class}.php") ){
        require_once ROOT."/app/models/{$class}.php";
    } 
});

//default helper
require_once ROOT."/app/helpers/Input.php";
require_once ROOT."/app/helpers/Session.php";
