<?php 

class Route {

    protected $controller = 'Home';
	protected $method = 'index';
	protected $params = [];

	public function __construct()
	{
        $url = $this->parseURL();
        
        //controller
		if(file_exists(ROOT.'/app/controllers/'.ucfirst(strtolower($url[0])).'.php') ){
			$this->controller = ucfirst($url[0]);
			unset($url[0]);
        } 
        
		require_once ROOT.'/app/controllers/'. ($this->controller).'.php';
        $this->controller = new $this->controller;
        
        //method
		if( isset($url[1]) ){
			if(method_exists($this->controller, $url[1])){
				$this->method = $url[1];
				unset($url[1]);
			}
		}

		//params
		if(!empty($url)){
			$this->params = array_values($url);
		}

        //for make sure that all values in $url is used in controller and method is unset 
        // var_dump($url);

		//run controller $method , and send params if params there
		call_user_func_array([$this->controller,$this->method], $this->params);

	}

	public function parseURL()
	{
		if(isset($_GET['url'])) {
			$url = rtrim($_GET['url'], '/');
			$url = filter_var($url,FILTER_SANITIZE_URL);
			$url = explode('/', $url);
			return $url;
		}
	}

}