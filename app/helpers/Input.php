<?php 

class Input {

    public static function get($name,$security=true)
    {
        switch($security){
            case true : 
            if(isset($_POST[$name])){
                return htmlspecialchars($_POST[$name]);
            } else if(isset($_GET[$name])){
                return htmlspecialchars($_GET[$name]);
            } else {
                return false;
            }
            break;

            case false :
            if(isset($_POST[$name])){
                return $_POST[$name];
            } else if(isset($_GET[$name])){
                return $_GET[$name];
            } else {
                return false;
            }
            break;
        }
    }

}