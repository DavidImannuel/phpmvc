<?php 

class Validate {
    
    private $errors = [],$passed = false;

    //give rule validation
    public function rules($input = [])
    {
        foreach($input as $name => $rules){
            foreach($rules as $rule => $rule_val){
                switch($rule){
                    case 'required' :
                        if(Input::get($name) == ''){
                            $this->addError($name,"$name is required");
                        }
                    break;
                    case 'min' :
                        if(strlen(Input::get($name)) < $rule_val){
                            $this->addError($name,"$name min $rule_val characters");
                        }
                    break; 
                    case 'max' :
                        if(strlen(Input::get($name)) > $rule_val){
                            $this->addError($name,"$name max $rule_val characters");
                        }
                    break; 
                }
            }
        }

       if(empty($this->errors)){
           $this->passed = true;
        //    var_dump($this->passed);die;
       } 
    }

    //for add error when validation running
    private function addError($name,$error)
    {
        if(!isset($this->errors[$name])){
            $this->errors[$name][] = $error;
        } else {
            array_push($this->errors[$name],$error);
        }
        
    }

    //check if validation passed
    public function passed()
    {
        return $this->passed;
    }

    //for get errors that added by addError() function
    public function getErrors()
    {
        return $this->errors;
    }


}