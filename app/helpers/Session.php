<?php 

class Session{
    
    //check if session exist
    public static function exists($name)
    {
        return (isset($_SESSION[$name])) ? true : false ;
    }

    //set session
    public static function set($name,$value)
    {
        return $_SESSION[$name] = $value;
    }

    //unset session
    public static function unset($name)
    {
        unset($_SESSION[$name]);
    }

    //get session
    public static function get($name,$value)
    {
        return $_SESSION[$name];
    }

    //set flash message
    public static function setFlash($message,$action,$type)
    {
        $_SESSION['flash'] = [
            'message' => $message,
            'action' => $action,
            'type' => $type,
        ];
    }

    //show flash message
    public static function flash()
    {
        if(isset($_SESSION['flash'])){
            echo "<div class='alert alert-{$_SESSION['flash']['type']} alert-dismissible fade show' role='alert'>
            <strong>{$_SESSION['flash']['message']}</strong> {$_SESSION['flash']['action']}
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
              <span aria-hidden='true'>&times;</span>
            </button>
          </div>";
        }

        unset($_SESSION['flash']);
    }

    //display error
    public static function errors($name)
    {
         if(isset($_SESSION['errors'][$name])) {
            echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>";
                foreach($_SESSION['errors'][$name] as $error) {
                    echo $error. "<br>";
                }
            echo "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
                </button>
            </div>";
            unset($_SESSION['errors'][$name]);
        }
    }
}