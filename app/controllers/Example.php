<?php 

//change 'Example' with your controller name , and naming for the controller must be end with 'Controller'  
class Example extends Controller {

    private $model;
    private $validate;

    public function __construct()
    {
        $this->model = $this->model('ExampleModel');
        $this->validate = $this->helper('Validate');
    }

    //index method set as default method , it must be there
    public function index()
    {
        $data['title'] = "Example CRUD";
        $data['rows'] = $this->model->all();
        $this->view('templates/header');
        $this->view('example/index',$data);
        $this->view('templates/footer');

    }

    public function create()
    {
        $this->view('templates/header');
        $this->view('example/create');
        $this->view('templates/footer');
    }

    public function insert()
    {
        $this->validate->rules([
            'name' => [
                'required' ,
                'min' => 3,
            ],
            'email' => [
                'required' ,
                'min' => 3,
            ],
            'city' => [
                'required' ,
                'min' => 3,
            ],
            'phone' => [
                'required' ,
                'min' => 3,
            ],
        ]);
        if($this->validate->passed()){
            $data = [
                'name' => Input::get('name'),
                'email' => Input::get('email'),
                'city' => Input::get('city'),
                'phone' => Input::get('phone'),
            ];
            // var_dump($data);
            $this->model->addData($data);
        } else {
            // var_dump($this->validate->getErrors());die;
            Session::set('errors',$this->validate->getErrors());
            // var_dump($_SESSION);die;
            header('Location: '.BASEURL.'example/create');
            exit;
        }        
    }

    public function edit($id)
    {
        $data = $this->model->single($id);
        $this->view('templates/header');
        $this->view('example/edit',$data);
        $this->view('templates/footer');
    }

    public function update($id)
    {
        $this->validate->rules([
            'name' => [
                'required' ,
                'min' => 3,
            ],
            'email' => [
                'required' ,
                'min' => 3,
            ],
            'city' => [
                'required' ,
                'min' => 3,
            ],
            'phone' => [
                'required' ,
                'min' => 3,
            ],
        ]);
        if($this->validate->passed()){
            $data = [
                'name' => Input::get('name'),
                'email' => Input::get('email'),
                'city' => Input::get('city'),
                'phone' => Input::get('phone'),
            ];
            // var_dump($data);
            $this->model->editData($data,$id);
        } else {
            // var_dump($this->validate->getErrors());die;
            Session::set('errors',$this->validate->getErrors());
            // var_dump($_SESSION);die;
            header('Location: '.BASEURL."example/edit/$id");
            exit;
        }        
    }

    public function delete($id)
    {
        $this->model->deleteData($id);
    }

}