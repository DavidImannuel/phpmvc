<?php  

class ExampleModel {

    private $table = 'example';
    private $db;

    public function __construct()
	{
        $this->db = new DB;
        // var_dump($this->db);
        
	}

    public function all()
	{
		return $this->db->resultAll($this->table);

    }

    public function single($id)
	{
		return $this->db->single($this->table,$id);
    }
    
    public function addData($data)
    {
        // var_dump($data);
        $this->db->insert($this->table,$data);
        Session::setFlash("Data", "was added","success");

        header('Location: '.BASEURL.'example');
        exit;
    }

    public function editData($data,$id)
    {
        // var_dump($data);
        $this->db->update($this->table,$data,$id);
        Session::setFlash("Data", "was edited","success");
        header('Location: '.BASEURL.'example');
        exit;
    }

    public function deleteData($id)
    {
        $this->db->delete($this->table,$id);
        Session::setFlash("Data","was deleted","success");
        header('Location: '.BASEURL.'example');
        exit;
    }

}