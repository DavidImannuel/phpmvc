<?php 

define('BASEURL', 'http://localhost/phpmvc/');
define('ASSET', 'http://localhost/phpmvc/public/');

function baseurl($url=''){
    return BASEURL.$url;
}
function asset($url=''){
    return ASSET.$url;
}

//DB
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_NAME', 'phpmvc');